// pages/tabview/tabview.js
Page({

  data: {
    // tabs: [],
    // tabs: ['我已报名', '我的发布'],
    // tabs: ['吕布', '赤兔马', '方天画戟'],
    // tabs: ['昨天', '今天', '明天', '后天'],
    tabs: ['全部', '待付款', '待发货', '待收货', '待评价'],
    currentIndex: 0
  },

  onLoad: function (options) {
    this.mytabview = this.selectComponent('#mytabview')
    this.setData({
      slotIds: this.mytabview.getSlotIds()
    })
  },

  pageChangeEvent: function (e) {
    console.log(e)
  },

  scrolltobottomEvent: function (e) {
    console.log(e)
  }

})